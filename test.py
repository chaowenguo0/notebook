import tensorflow

num_inputs = 2
num_examples = 1000
true_w = [2, -3.4]
true_b = 4.2
features = tensorflow.random.normal(shape=(num_examples, num_inputs), stddev=1)
labels = true_w[0] * features[:, 0] + true_w[1] * features[:, 1] + true_b
labels += tensorflow.random.normal(labels.shape, stddev=0.01)
print(features[:,0].numpy().tolist())
